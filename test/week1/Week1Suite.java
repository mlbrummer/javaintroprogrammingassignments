/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author michiel
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({week1_1.BmiCalculatorTest.class, week1_2.AllSubstringsPrinterTest.class, week1_3.WeightUnitsSolverTest.class})
public class Week1Suite {
    
}
